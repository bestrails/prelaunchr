# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'prelaunchr'

set :repo_url, 'git@bitbucket.org:bestrails/prelaunchr.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_via, :remote_cache

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
set :pty, true

set :use_sudo, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/assets', 
 'vendor/bundle', 'public/system', 'public/uploads')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5


before "deploy:symlink:release", "maintenance:post_deploy"

namespace :maintenance do

 desc "Post Deploy steps"
 task :post_deploy do
  invoke 'maintenance:bundle_install'
  invoke 'maintenance:assets_precompile'
  invoke 'maintenance:migrate_db'
 end

 desc "Bundle install"
 task :bundle_install do
  on roles(:web) do
   rails_env = fetch(:rails_env)
   release_path = fetch(:release_path)
   execute "cd #{release_path} && bash -lc 'RAILS_ENV=#{rails_env} bundle install --deployment'", raise_on_non_zero_exit: false
  end
 end

 desc "Runs db migrate"
 task :migrate_db do
  on roles(:db) do
   rails_env = fetch(:rails_env)
   release_path = fetch(:release_path)

   # Capistrano fails if the exit from shell is anything other than 0, so we suppress errors
   execute "cd #{release_path} && bash -lc 'RAILS_ENV=#{rails_env} bundle exec rake db:migrate'", raise_on_non_zero_exit: false
   execute "cd #{release_path} && bash -lc 'RAILS_ENV=#{rails_env} bundle exec rake imports:hbcount'", raise_on_non_zero_exit: false
  end
 end

 desc "Rakes assets"
 task :assets_precompile do

  on roles(:app) do
   rails_env = fetch(:rails_env)
   release_path = fetch(:release_path)

   # No need to clobber assets in Rails 4, since assets:precompile only runs on new/changed assets.
   execute "cd #{release_path} && bash -lc 'RAILS_ENV=#{rails_env} bundle exec rake assets:precompile --trace'", raise_on_non_zero_exit: false
  end
 end
  
 
 desc "Restart Puma"
 task :restart_puma do
  on roles(:web) do
   execute :sudo, "restart puma-manager", raise_on_non_zero_exit: false
  end
 end
end

after 'deploy:publishing', 'deploy:restart'

namespace :deploy do

 desc "Restart application"
 task :restart do
  on roles(:web), in: :sequence, wait: 5 do
   execute :sudo, "restart puma-manager", raise_on_non_zero_exit: false
  end
 end

 after :finishing, "deploy:cleanup"
end
